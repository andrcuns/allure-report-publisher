# allure-report-publisher

[CI/CD component](https://docs.gitlab.com/ee/ci/components/index.html) for publishing [allure](https://allurereport.org/) reports using [allure-report-publisher](https://github.com/andrcuns/allure-report-publisher) tool.

## Usage

Minimal configuration is simply including the component and providing cloud provider type and bucket name:

```yaml
include:
  - component: gitlab.com/andrcuns/allure-report-publisher/template@2.0.0
    inputs:
      storage_type: s3
      bucket: allure-reports
```

This configuration will add a job called `publish-allure-report` in `.post` stage.

### Authentication

#### Cloud providers

Authentication requires defining specific environment variables depending on the cloud provider type. Documentation on supported
environment variables can be found [here](https://github.com/andrcuns/allure-report-publisher#storage-providers).

#### GitLab

If `inputs.update_mr` is set to `comment` or `description`, the job will update the merge request with the link to the published report. This requires api token with `api` scope. Token can be set via `inputs.ci_api_token` or `GITLAB_AUTH_TOKEN` environment variable.

More information on GitLab authentication can be found [here](https://github.com/andrcuns/allure-report-publisher#authentication)

#### Job local environment variables

If you can't define AWS or GCS specific environment variables globally, you can define them locally in the job by appending variables section to the job definition:

```yaml
publish-allure-report:
  variables:
    AWS_ACCESS_KEY_ID: $MY_ALLURE_SPECIFIC_AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $MY_ALLURE_SPECIFIC_AWS_SECRET_ACCESS_KEY
```

### Supporting multiple projects and merge requests

If you want to support publishing reports from different projects and merge requests, it is useful to configure custom path prefix
that consists of project name and branch name.

```yaml
include:
  - component: gitlab.com/andrcuns/allure-report-publisher/template@2.0.0
    inputs:
      storage_type: s3
      bucket: allure-reports
      path_prefix: $CI_PROJECT_NAME/$CI_COMMIT_REF_SLUG
      update_mr: "true"
```

### Configuration

* For a full list of available inputs, see [template.yml](templates/template.yml) definition.
* For a full list of command line arguments, see [allure-report-publisher](https://github.com/andrcuns/allure-report-publisher#usage).
